FROM ubuntu:latest
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y sudo
COPY sudoers /etc/sudoers
RUN adduser ubuntu -q --disabled-password
RUN adduser ubuntu sudo
RUN passwd --delete ubuntu
USER ubuntu
CMD ["bash"]
