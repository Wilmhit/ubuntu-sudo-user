# ubuntu-sudo-user

This is a Dockerfile project that creates a ubuntu image with "ubuntu" user and
sudo utility.

### Usage

All commands run as unprivileged user. In order to run command as root use
`sudo`.

### Explanation

This container was build in order to somewhat simulate cloud provider's ubuntu
images. You can test you ansible playbooks on those containers.

### Repository

Main repository for this dockerfile is located at
[gitlab.com/Wilmhit/ubuntu-sudo-user](https://gitlab.com/Wilmhit/ubuntu-sudo-user)
